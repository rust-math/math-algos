//! Calculating pi using Chudnovsky algorithm
//! https://en.wikipedia.org/wiki/Chudnovsky_algorithm

use num_traits::*;

/// Calculate an approximate value of pi.
///
/// *to_k*: Go up to variable *k* in precision.
/// *c*: An approximate of `426880*sqrt(10005)`. More precision here means more
/// precision in the final output.
pub fn pi<N, T>(to_k: i32, c: T) -> T
    where
        N: From<i32>,
        T: NumAssign + From<N> + Pow<u16, Output = T> + Clone + Inv<Output = T>
{
    let mut sum: T = T::zero();

    let mut kk: T = T::from(N::from(6));
    let mut l: T = T::from(N::from(13591409));
    let mut m: T = T::one();
    let mut x: T = T::one();
    for k in 0..=to_k {
        sum += m.clone() * l.clone() / x.clone();

        l += T::from(N::from(545140134));
        // factorized -262537412640768000 to fit an i32
        x *= T::from(N::from(-884736000)) * T::from(N::from(296740963));

        kk += T::from(N::from(16));
        m *= (kk.clone().pow(3) - T::from(N::from(16))*kk.clone()) / (T::from(N::from(k)) + T::one()).pow(3);
    }

    sum.inv() * c
}

#[cfg(test)]
mod tests {
    use super::*;
    use math_types::BigInt;

    type Fraction = math_types::Fraction<BigInt>;

    #[test]
    fn main() {
        assert!((pi::<f64, f64>(1, 42698670.66633339) - 3.141592653594799).abs() < std::f64::EPSILON);
    }
    #[test]
    fn bigint() {
        assert_eq!(
            pi::<BigInt, Fraction>(0, Fraction::new(
                BigInt::new(4269867066633339i64),
                BigInt::new(100000000)
            )),
            Fraction::new(
                BigInt::from(4269867066633339i64),
                BigInt::from(1359140900000000i64)
            )
        );
        assert_eq!(
            pi::<BigInt, Fraction>(1, Fraction::new(
                BigInt::new(4269867066633339i64),
                BigInt::new(100000000)
            )),
            Fraction::new(
                BigInt::from(486545074650148678181935488i128),
                BigInt::from(154872107334926009641721875i128)
            )
        );
    }
}
