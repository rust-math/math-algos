//! Calculating gamma using Spouge's approximation
//! https://en.wikipedia.org/wiki/Spouge%27s_approximation

use num_traits::*;

/// Get a list of all gamma coefficients up to `a - 1`
pub fn gamma_coefficients<N>(a: u32) -> Vec<N>
    where N: Float + NumAssignRef + FloatConst + Pow<N, Output = N>
{
    assert!(a <= std::i32::MAX as u32 + 1, "a must be less than or equal to i32::MAX + 1");

    let mut vec = Vec::new();
    if a > 0 {
        vec.push((N::from(2).unwrap() * N::PI()).sqrt());
    }
    for k in 1..a {
        let a = N::from(a).unwrap();
        let k = N::from(k).unwrap();
        vec.push(
            ((-N::one()).pow(k - N::one())) / faci(k - N::one())
                * (a - k).pow(k - (N::one() / N::from(2).unwrap()))
                * (a - k).exp()
        );
    }
    vec
}

/// Return the gamma with the specified coefficients. Consider using gamma for
/// the default coefficients with ~10 digits of precision.
pub fn gamma_with_coefficients<N>(mut z: N, c: &[N]) -> N
    where N: Float + NumAssignRef + FloatConst + Pow<N, Output = N>
{
    assert!(!c.is_empty(), "coefficients can't be empty");

    z -= N::one();

    // Smaller values have higher precision
    let mut multiply = N::one();
    while z >= N::one() {
        multiply *= z;
        z -= N::one();
    }

    if z.is_zero() {
        return multiply;
    }

    let a = N::from(c.len()).unwrap();
    let mut sum = c[0];
    for (k, &c) in c.iter().enumerate() {
        sum += c / (z + N::from(k).unwrap()) /* + std::f64::EPSILON * a */;
    }
    let result = (z + a).pow(z + (N::one() / N::from(2).unwrap())) * (-z - a).exp() * sum;

    let scale = N::from(100000000i64).unwrap();
    (result * multiply * scale).floor() / scale
}

thread_local! {
    /// Default coefficients that provide ~10 digits of precision
    pub static DEFAULT_COEFFICIENTS: Vec<f64> = gamma_coefficients(30);
}

/// Convenience gamma function that uses the default coefficients for ~8 digits of precision
pub fn gamma(z: f64) -> f64 {
    DEFAULT_COEFFICIENTS.with(|coefficients|
        gamma_with_coefficients(z, coefficients)
    )
}

/// Get the result of an integer factorial
pub fn faci<N>(mut n: N) -> N
    where N: NumAssignRef + PartialOrd
{
    let mut result = N::one();
    while n >= N::one() {
        result *= &n;
        n -= N::one();
    }
    result
}

/// Convenience function for calling gamma(n + 1)
pub fn facf(n: f64) -> f64 {
    gamma(n + 1.0)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn main() {
        let precision = 1f64.powi(-10);
        assert!((facf(0.1) - 0.9513507699).abs() < precision);
        assert!((facf(0.001) - 0.9994237725).abs() < precision);
        assert!((facf(0.5) - 0.8862269255).abs() < precision);
        assert!((facf(0.0) - 1.0).abs() < precision);
        assert!((facf(13.43) - 19200308406.0).abs() < 1.0);

        let mut result: u128 = 1;
        for i in 1..=24 {
            result *= i;
            assert!((result as f64 - facf(i as f64)).abs() < 1.0, "failed at {}!", i);
        }
    }
}
