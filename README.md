# math-aglos

This is a WIP crate designed to hold math algorithms. Want to approximate pi?
This crate *should* probably support that some time.

Currently it only supports the gamma function (and can be used for non-integral
factorial).
